# Headlight

IOS mobile application - Finding suitable course path for chosen career

#### Technologies:

| Swift | XCode |




### What is Headlight?

- Purpose of Headlight is to find a suitable course path for a selected career
- Works with a defined algorithm to determine the best possible path for you
- Easy to use and understand
- You can also search and complete individual courses, if you’re interested

### Target user group for Headlight:

- People who want to change their current careers
- People who want to start a new career
- App supports Finnish/English language



# App demo:
###  Registration flow
- Enter your name
- Choose the skills you already know

![alt text][signIn]
---

### Career path
Create a personalized course list based on the career you have selected and the skills you already have
- Select a career path
- Preview the personal list of courses created for you

![alt text][selectCareer] ![alt text][career]
---

### Overview

On the main page you can..
- follow your current progress
- search for courses
- see your upcoming/ongoing courses

![alt text][overview]
---

### Search

Detailed course search can be done on the search page
- Search by name
- Quick search

![alt text][search]
---

### Course information

Detailed course information can be seen on each individual course page.
- Name
- Rating
- Description
- Dates
- Location
- Skills

![alt text][courseInfo] ![alt text][location]






[signIn]: https://gitlab.com/ischoolmusical/headlight/raw/master/demo/signin_300.gif "Signing in"
[selectCareer]: https://gitlab.com/ischoolmusical/headlight/raw/master/demo/select_career_300.png "Select career"
[career]: https://gitlab.com/ischoolmusical/headlight/raw/master/demo/career_300.png "Career"
[overview]: https://gitlab.com/ischoolmusical/headlight/raw/master/demo/overview_300.gif "Overview"
[search]: https://gitlab.com/ischoolmusical/headlight/raw/master/demo/search_300.gif "Search"
[courseInfo]: https://gitlab.com/ischoolmusical/headlight/raw/master/demo/course_info_300.png "Course information"
[location]: https://gitlab.com/ischoolmusical/headlight/raw/master/demo/location_300.png "Course location"


